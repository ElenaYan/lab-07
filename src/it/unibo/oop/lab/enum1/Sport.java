/**
 * 
 */
package it.unibo.oop.lab.enum1;

//import it.unibo.oop.lab.nesting1.SportSocialNetworkUserImpl.Sport;

/**
 * Represents an enumeration for declaring sports;
 * 
 * 1) Complete the definition of the enumeration.
 * 
 */
public enum Sport {
    /*
     * declare the following sports: - basket - soccer - tennis - bike - F1 -
     * motogp - volley
     */
	BASKET,SOCCER,TENNIS,BIKE,F1,MOTOGP,VOLLEY;
}
